﻿"use strict";
var selectPaymentMethod = function (ele) {
    if (!ele.hasClass("selected")) {
        var eleClass = ele.attr("class");
        $("." + eleClass).each(function () {
            $(this).attr("data-checked","false;");
            $(this).removeClass("selected");
        });
        ele.addClass("selected");
        ele.attr("data-checked", "true");
    }
}

var setNotification = function(ele) {
    if (!ele.hasClass("selected")) {
        ele.addClass("selected");
        ele.attr("data-checked","true");
    } else {
        ele.removeClass("selected");
        ele.attr("data-checked", "false");
    }
}